package com.epam.rd.june2018.tree;

import com.epam.rd.june2018.BinaryTree;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.hamcrest.CoreMatchers.is;

public class BinaryTreeTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testConstructorWithMap() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<>();
        tree1.put("A", "Aunt");
        tree1.put("B", "Brother");
        int expected = 2;
        //WHEN
        BinaryTree<String, String> tree2 = new BinaryTree<>(tree1);
        int actual = tree2.size();
        //THEN
        Assert.assertNotNull(tree2);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testSize() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        int expected = 0;
        //WHEN
        int actual = tree1.size();
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testPutWithNullKey() {
        expectedException.expect(NullPointerException.class);
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        tree1.put(null, "Aunt");
    }

    @Test
    public void testPutWithNullValue() {
        expectedException.expect(NullPointerException.class);
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        tree1.put("A", null);
    }

    @Test
    public void testPutWithNullKeyAndValue() {
        expectedException.expect(NullPointerException.class);
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        tree1.put(null, null);
    }

    @Test
    public void testPutAndSize() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        int expected = 2;
        //WHEN
        tree1.put("A", "Aunt");
        tree1.put("B", "Brother");
        int actual = tree1.size();
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testPutAndSize1() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        int expected = 2;
        //WHEN
        tree1.put("A", "Apple");
        tree1.put("A", "Aunt");
        tree1.put("B", "Brother");
        int actual = tree1.size();
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testGetWithNullKey() {
        expectedException.expect(NullPointerException.class);
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        tree1.put("A", "Aunt");
        tree1.get(null);
    }

    @Test
    public void testGetFromEmptyBinaryTree() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        //WHEN
        String actual = tree1.get("A");
        //THEN
        Assert.assertNull(actual);
    }

    @Test
    public void testGet() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        String expectedA = "Aunt";
        String expectedB = "Brother";
        String expectedC = "Cousin";
        tree1.put("A", "Aunt");
        tree1.put("B", "Brother");
        tree1.put("C", "Cousin");
        //WHEN
        String stringA = tree1.get("A");
        String stringB = tree1.get("B");
        String stringC = tree1.get("C");
        //THEN
        Assert.assertNotNull(expectedA);
        Assert.assertNotNull(expectedB);
        Assert.assertNotNull(expectedC);
        Assert.assertThat(stringA, is(expectedA));
        Assert.assertThat(stringB, is(expectedB));
        Assert.assertThat(stringC, is(expectedC));
    }

    @Test
    public void testEqualsItself() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        //WHEN
        tree1.put("A", "Aunt");
        tree1.put("B", "Brother");
        tree1.put("C", "Cousin");
        Boolean actual = tree1.equals(tree1);
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertTrue(actual);
    }

    @Test
    public void testEqualsWithNull() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<String, String>();
        BinaryTree tree2 = null;
        //WHEN
        tree1.put("A", "Aunt");
        tree1.put("B", "Brother");
        tree1.put("C", "Cousin");
        Boolean actual = tree1.equals(tree2);
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertNull(tree2);
        Assert.assertFalse(actual);
    }

    @Test
    public void testEqualsTrue() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<>();
        BinaryTree<String, String> tree2 = new BinaryTree<String, String>();
        //WHEN
        tree1.put("A", "Aunt");
        tree1.put("B", "Brother");
        tree1.put("C", "Cousin");
        tree2.put("A", "Aunt");
        tree2.put("B", "Brother");
        tree2.put("C", "Cousin");
        Boolean actual12 = tree1.equals(tree2);
        Boolean actual21 = tree1.equals(tree2);
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertNotNull(tree2);
        Assert.assertTrue(actual12);
        Assert.assertTrue(actual21);
    }

    @Test
    public void testEqualsFalse() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<>();
        BinaryTree<String, String> tree2 = new BinaryTree<>();
        //WHEN
        tree1.put("A", "Aunt");
        tree1.put("B", "Brother");
        tree1.put("C", "Cousin");
        tree2.put("A", "Aunt");
        tree2.put("B", "Brother");
        //THEN
        Assert.assertNotNull(tree1);
        Assert.assertNotNull(tree2);
        Assert.assertTrue(tree1.equals(tree1));
        Assert.assertTrue(tree2.equals(tree2));
        Assert.assertFalse(tree1.equals(tree2));
        Assert.assertFalse(tree2.equals(tree1));
    }

    @Test
    public void testHashCode() {
        //GIVEN
        BinaryTree<String, String> tree1 = new BinaryTree<>();
        BinaryTree<String, String> tree2 = new BinaryTree<String, String>();
        //WHEN
        tree1.put("A", "Aunt");
        tree1.put("B", "Brother");
        tree1.put("C", "Cousin");
        tree2.put("A", "Aunt");
        tree2.put("B", "Brother");
        tree2.put("C", "Cousin");
        //THEN
        Assert.assertEquals(tree1.hashCode(), tree2.hashCode());
    }
}