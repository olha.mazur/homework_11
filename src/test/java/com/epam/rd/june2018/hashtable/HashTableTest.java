package com.epam.rd.june2018.hashtable;

import com.epam.rd.june2018.HashTable;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;

public class HashTableTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testConstructorWithMap() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<String, String>();
        table1.put("A", "Aunt");
        table1.put("B", "Brother");
        int expected = 2;
        //WHEN
        HashTable<String, String> table2 = new HashTable<String, String>(table1);
        int actual = table2.size();
        //THEN
        Assert.assertNotNull(table2);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testSize() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<String, String>();
        int expected = 0;
        //WHEN
        int actual = table1.size();
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testPutWithNullKey() {
        expectedException.expect(NullPointerException.class);
        HashTable<String, String> table1 = new HashTable<String, String>();
        table1.put(null, "Aunt");
    }

    @Test
    public void testPutWithNullValue() {
        expectedException.expect(NullPointerException.class);
        HashTable<String, String> table1 = new HashTable<String, String>();
        table1.put("A", null);
    }

    @Test
    public void testPutWithNullKeyAndValue() {
        expectedException.expect(NullPointerException.class);
        HashTable<String, String> table1 = new HashTable<String, String>();
        table1.put(null, null);
    }

    @Test
    public void testPutAndSize() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<String, String>();
        int expected = 2;
        //WHEN
        table1.put("A", "Aunt");
        table1.put("B", "Brother");
        int actual = table1.size();
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testPutAndSize1() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<String, String>();
        int expected = 2;
        //WHEN
        table1.put("A", "Apple");
        table1.put("A", "Aunt");
        table1.put("B", "Brother");
        int actual = table1.size();
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void testGetWithNullKey() {
        expectedException.expect(NullPointerException.class);
        HashTable<String, String> table1 = new HashTable<String, String>();
        table1.put("A", "Aunt");
        table1.get(null);
    }

    @Test
    public void testGetFromEmptyHashTable() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<String, String>();
        //WHEN
        String actual = table1.get("A");
        //THEN
        Assert.assertNull(actual);
    }

    @Test
    public void testGet() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<String, String>();
        String expectedA = "Aunt";
        String expectedB = "Brother";
        String expectedC = "Cousin";
        table1.put("A", "Aunt");
        table1.put("B", "Brother");
        table1.put("C", "Cousin");
        //WHEN
        String stringA = table1.get("A");
        String stringB = table1.get("B");
        String stringC = table1.get("C");
        //THEN
        Assert.assertNotNull(expectedA);
        Assert.assertNotNull(expectedB);
        Assert.assertNotNull(expectedC);
        Assert.assertThat(stringA, is(expectedA));
        Assert.assertThat(stringB, is(expectedB));
        Assert.assertThat(stringC, is(expectedC));
    }

    @Test
    public void testEqualsItself() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<String, String>();
        //WHEN
        table1.put("A", "Aunt");
        table1.put("B", "Brother");
        table1.put("C", "Cousin");
        Boolean actual = table1.equals(table1);
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertTrue(actual);
    }

    @Test
    public void testEqualsWithNull() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<String, String>();
        HashTable table2 = null;
        //WHEN
        table1.put("A", "Aunt");
        table1.put("B", "Brother");
        table1.put("C", "Cousin");
        Boolean actual = table1.equals(table2);
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertNull(table2);
        Assert.assertFalse(actual);
    }

    @Test
    public void testEqualsTrue() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<>();
        HashTable<String, String> table2 = new HashTable<String, String>();
        //WHEN
        table1.put("A", "Aunt");
        table1.put("B", "Brother");
        table1.put("C", "Cousin");
        table2.put("A", "Aunt");
        table2.put("B", "Brother");
        table2.put("C", "Cousin");
        Boolean actual12 = table1.equals(table2);
        Boolean actual21 = table1.equals(table2);
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertNotNull(table2);
        Assert.assertTrue(actual12);
        Assert.assertTrue(actual21);
    }

    @Test
    public void testEqualsFalse() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<>();
        HashTable<String, String> table2 = new HashTable<>();
        //WHEN
        table1.put("A", "Aunt");
        table1.put("B", "Brother");
        table1.put("C", "Cousin");
        table2.put("A", "Aunt");
        table2.put("B", "Brother");
        //THEN
        Assert.assertNotNull(table1);
        Assert.assertNotNull(table2);
        Assert.assertTrue(table1.equals(table1));
        Assert.assertTrue(table2.equals(table2));
        Assert.assertFalse(table1.equals(table2));
        Assert.assertFalse(table2.equals(table1));
    }

    @Test
    public void testHashCode() {
        //GIVEN
        HashTable<String, String> table1 = new HashTable<>();
        HashTable<String, String> table2 = new HashTable<String, String>();
        //WHEN
        table1.put("A", "Aunt");
        table1.put("B", "Brother");
        table1.put("C", "Cousin");
        table2.put("A", "Aunt");
        table2.put("B", "Brother");
        table2.put("C", "Cousin");
        //THEN
        Assert.assertEquals(table1.hashCode(), table2.hashCode());
    }
}