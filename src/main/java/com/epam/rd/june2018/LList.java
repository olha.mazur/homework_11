package com.epam.rd.june2018;

import java.util.Iterator;

public class LList<T> implements IList1<T> {
    Node root = null;

    class Node {
        T value;
        Node next = null;
        public Node (T value)
        {
            this.value = value;
        }
    }

    public LList() {

    }

    public LList(int size) {
        for (int i = 0; i < size; i++) {
            addFirst(null);
        }
    }

    @Override
    public void clear() {
        root = null;
    }

    @Override
    public int size() {
        int count = 0;
        Node tmp = root;
        while (tmp!=null) {
            tmp = tmp.next;
            count++;
        }
        return count;
    }

    @Override
    public boolean add(T value) {
        addLast(value);
        return true;
    }

    @Override
    public void addFirst(T value) {
        Node tmp = new Node(value);
        tmp.next = root;
        root = tmp;
    }

    @Override
    public void addLast(T value) {
        if (root == null) {
            root = new Node(value);
        } else {
            Node tmp = root;
            while (tmp.next!=null) {
                tmp = tmp.next;
            }
            tmp.next = new Node(value);
        }
    }

    @Override
    public void add(int index, T value) {
        if (index < 0 || index > size()) {
            throw new IllegalArgumentException("Cannot add an elements with index " + index);
        }
        if(root == null || index == 0) {
            addFirst(value);
        } else {
            int count = 0;
            Node tmp = root;
            while (count < index-1) {
                tmp = tmp.next;
                count++;
            }
            Node tmp1 = new Node(value);
            tmp1.next = tmp.next;
            tmp.next = tmp1;
        }
    }

    @Override
    public T removeFirst() {
        if (root == null) {
            throw new IllegalStateException("There is no elements in the list!");
        }
        T res = root.value;
        root = root.next;
        return res;
    }

    @Override
    public T removeLast() {
        if (root == null) {
            throw new IllegalStateException("There is no elements in the list!");
        }
        T res = root.value;
        if (size() == 1) {
            clear();
        } else {
            Node tmp = root;
            while (tmp.next.next!=null) {
                tmp = tmp.next;
            }
            res = tmp.next.value;
            tmp.next = null;
        }
        return res;
    }

    @Override
    public T remove(int index) {
        if (root == null || index < 0 || index >size()-1) {
            throw new IllegalArgumentException("Cannot remove an elements with index " + index);
        }
        T result = root.value;
        if (size() == 1) {
            clear();
        } else if(index == 0) {
            result = removeFirst();
        } else {
            int count = 0;
            Node tmp = root;
            while (count < index-1) {
                tmp = tmp.next;
                count++;
            }
            result = tmp.next.value;
            tmp.next = tmp.next.next;
        }
        return result;
    }

    @Override
    public T getFirst() {
        return get(0);
    }

    @Override
    public T getLast() {
        return get(size() - 1);
    }

    @Override
    public void setFirst(T value) {
        set(0, value);
    }

    @Override
    public void setLast(T value) {
        set(size() - 1, value);
    }

    @Override
    public T get(int index) {
        if (root == null || index < 0 || index > size()-1) {
            throw new IllegalArgumentException("The list has no element with index " + index);
        }
        T result = null;
        int count = 0;
        Node tmp = root;
        while (tmp!=null) {
            if (index == count) {
                result = tmp.value;
            }
            tmp = tmp.next;
            count++;
        }
        return result;
    }

    @Override
    public void set(int index, T value) {
        if (root == null || index < 0 || index > size()-1) {
            throw new IllegalArgumentException("The list has no element with index " + index);
        }
        int count = 0;
        Node tmp = root;

        while (tmp!=null) {
            if (index == count){
                tmp.value = value;
            }
            tmp = tmp.next;
            count++;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return (Iterator<T>) new MyListIter(root);
    }

    class MyListIter implements Iterator<T> {
        Node p = null;

        public MyListIter(Node p) {
            this.p = p;
        }

        @Override
        public boolean hasNext() {
            return (p != null);
        }

        @Override
        public T next() {
            T result = p.value;
            p = p.next;
            return result;
        }
    }

    @Override
    public boolean equals(Object o) {
        if(this == o) {
            return true;
        }
        if(o == null || !(o instanceof LList)) {
            return false;
        }

        LList lst = (LList) o;
        if(size() != lst.size()) {
            return false;
        }
        Node tmp1 = root;
        Node tmp2 = lst.root;
        for (int i = 0; i < size(); i++) {
            T p1 = tmp1.value;
            T p2 = tmp2.value;
            if(!(p1.equals(p2)))
                return false;
            tmp1 = tmp1.next;
            tmp2 = tmp2.next;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return root != null ? root.hashCode() : 0;
    }

    public Object[] toArray()
    {
        Object[] res = new Object[size()];
        Node tmp = root;
        for (int i = 0; i < res.length; i++)
        {
            res[i] = tmp.value;
            tmp = tmp.next;
        }
        return res;
    }
}
