package com.epam.rd.june2018;

import com.epm.lab.collections.Map;
import com.epm.lab.collections.OrderedMap;
import java.util.Iterator;

public class BinaryTree<K extends Comparable<K>, V> implements OrderedMap<K, V> {

    public Node root;

    public class Node
    {
        Entry<K, V> entry;
        Node left;
        Node right;
        byte height;

        public Node (Entry<K, V> e)
        {
            entry = e;
            height = 1;
            left = null;
            right = null;
        }
    }

    public BinaryTree(){
        root = null;
    }

    public BinaryTree(Map<K, V> map){
        for (Entry<K, V> entry : map) {
            put(entry.key, entry.value);
        }
    }

    @Override
    public V get(K k) {
        if (root == null){
         return null;
        }
        Node tmp = root;
        while (tmp.entry.key != k)
        {
            if(k.compareTo(tmp.entry.key) < 0) {
                tmp = tmp.left;
            } else {
                tmp = tmp.right;
            }
        }
        return tmp.entry.value;
    }

    @Override
    public void put(K k, V v) {
        if (root == null)
        {
            root = new Node(new Entry<>(k, v));
            return;
        }
        addNode(root,new Entry<>(k, v));
    }

    private void addNode(Node p, Entry<K, V> e)
    {
        if (e.key.compareTo(p.entry.key) == 0) {
            p.entry.value = e.value;
        } else {
            if (e.key.compareTo(p.entry.key) < 0) {
                if (p.left == null)
                    p.left = new Node(e);
                else
                    addNode(p.left, e);
            } else {
                if (p.right == null)
                    p.right = new Node(e);
                else
                    addNode(p.right, e);
            }
        }
    }

    @Override
    public int size()
    {
        return sizeNode(root);
    }

    private int sizeNode(Node p)
    {
        if(p == null)
            return 0;
        int count = 0;
        count += sizeNode(p.left);		//left
        count++;						//v
        count += sizeNode(p.right);		//right
        return count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BinaryTree)) return false;

        BinaryTree<K, V> that = (BinaryTree<K, V>) o;
        return  equalsNode(root, that.root);
    }

    private boolean equalsNode(Node a, Node b)
    {
        if(a == null && b == null)
            return true;
        else if(a != null && b != null)
            return ((a.entry).equals(b.entry))&&(equalsNode(a.left, b.left))&&(equalsNode(a.right,b.right));
        else
            return false;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        for (Entry<K, V> entry : entrySet()) {
            hashCode += (entry.key.hashCode() & Integer.MAX_VALUE);
        }
        return hashCode;
    }

    private LList<Entry<K, V>> entrySet(){
        LList<Entry<K, V>> entryList = new LList<>();
        nodeEntrySet(root,entryList);
        return entryList;
    }

    private LList<Entry<K, V>> nodeEntrySet(Node p, LList<Entry<K, V>> list){
        if(p == null) {
            return null;
        }
        nodeEntrySet(p.left, list);
        list.add(p.entry);
        nodeEntrySet(p.right, list);
        return list;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return entrySet().iterator();
    }
}
