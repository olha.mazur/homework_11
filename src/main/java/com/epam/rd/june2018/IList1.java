package com.epam.rd.june2018;

public interface IList1<T> extends IList<T> {
    void  addFirst(T value);
    void  addLast(T value);
    T   removeFirst();
    T   removeLast();

    T   getFirst();
    T   getLast();
    void  setFirst(T value);
    void  setLast(T value);
}
