package com.epam.rd.june2018;

import com.epm.lab.collections.Map;

import java.util.Iterator;

public class HashTable<K, V> implements Map<K, V> {

    private IList1<Entry<K, V>>[] array;// = new LList[11];
    private int size = 0;

    public HashTable(){
        array = new LList[11];
    }

    public HashTable(Map<K, V> map){
        array = new LList[map.size()*2];

        for (Entry<K, V> entry : map) {
            put(entry.key, entry.value);
        }
    }

    @Override
    public V get(K key) {
        if(key == null)
            throw new NullPointerException();
        int index = (key.hashCode() & Integer.MAX_VALUE) % array.length;
        if (array[index] != null) {
            for (Entry<K, V> entry : array[index]) {
                if (entry.key.equals(key)){
                    return  entry.value;
                }
            }
        }
        return null;
    }

    @Override
    public void put(K key, V value) {
        if(key == null || value == null)
            throw new NullPointerException();

        int index = (key.hashCode() & Integer.MAX_VALUE) % array.length;
        if (array[index] != null) {
            for (Entry<K, V> entry : array[index]) {
                if (entry.key.equals(key)){
                    entry.value = value;
                    return;
                }
            }
        } else {
            array[index] = new LList<>();
        }
        array[index].add(new Entry(key, value));
        size++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || !(o instanceof HashTable)){
            return false;
        }

        HashTable<?, ?> hashTable = (HashTable<?, ?>) o;
        if(array.length != hashTable.array.length){
            return false;
        }
        if(size() != hashTable.size) {
            return false;
        }
        for (int i = 0; i < array.length; i++)
        {
            if(array[i] != null && hashTable.array[i] != null && !(array[i].equals(hashTable.array[i])))
                return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        for (Entry<K, V> entry : entrySet()) {
            hashCode += (entry.key.hashCode() & Integer.MAX_VALUE);
        }
        return hashCode;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return entrySet().iterator();
    }

    private LList<Entry<K, V>> entrySet(){
        LList<Entry<K, V>> entryList = new LList<>();
        for (int i = 0; i < array.length; i++) {
            if(array[i] != null) {
                for (Entry<K, V> entry : array[i]) {
                    entryList.add(new Entry<>(entry.key, entry.value));
                }
            }
        }
        return entryList;
    }
}
