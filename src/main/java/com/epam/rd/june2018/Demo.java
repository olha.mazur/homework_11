package com.epam.rd.june2018;

import com.epm.lab.collections.Map;

public class Demo
{
    public static void main( String[] args )
    {
        Map<String, String> phoneBook = new HashTable<>();

        if (args.length > 0 && "sorted".equals(args[0])){
            phoneBook = new BinaryTree<>();
        }

        phoneBook.put("TV",  "322223");
        phoneBook.put("Vasya Pupkin",  "111-11-11");
        phoneBook.put("Masya Pupkina", "222-22-22");

        String[] keys = new String[phoneBook.size()];

        int i = 0;
        for (Map.Entry<String, String> phoneNumber: phoneBook) {
            keys[i++] = phoneNumber.key;
        }

        for (String key : keys) {
            System.out.println("Name: " + key + ", phone number:" + phoneBook.get(key));
        }
    }
}
