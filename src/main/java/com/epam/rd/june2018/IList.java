package com.epam.rd.june2018;

public interface IList<T> extends Iterable<T>{
    void clear();
    int   size();

    boolean add(T value);
    void  add(int index, T value);
    T   remove(int index);

    T   get(int index);
    void  set(int index, T value);
}
